package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/asiainfoLDP/openshift-operator/client/crd"
	applogger "github.com/asiainfoLDP/openshift-operator/log"
	apiextensionscli "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	podtermk8scli "github.com/asiainfoLDP/openshift-operator/mysql-operator/client/k8s/clientset/versioned"
	"github.com/asiainfoLDP/openshift-operator/mysql-operator/log"
	"github.com/asiainfoLDP/openshift-operator/mysql-operator/operator"
	"flag"
	"path/filepath"
	"k8s.io/client-go/util/homedir"
)

// Main is the main program.
type Main struct {
	flags  *Flags
	config operator.Config
	logger log.Logger
}

// New returns the main application.
func New(logger log.Logger) *Main {
	fmt.Println("-----in-")
	f := NewFlags()
	return &Main{
		flags:  f,
		config: f.OperatorConfig(),
		logger: logger,
	}
}

// Run runs the app.
func (m *Main) Run(stopC <-chan struct{}) error {
	m.logger.Infof("initializing pod termination operator")

	// Get kubernetes rest client.
	ptCli, crdCli, k8sCli, err := m.getKubernetesClients()
	if err != nil {
		return err
	}

	// Create the operator and run
	op, err := operator.New(m.config, ptCli, crdCli, k8sCli, m.logger)
	if err != nil {
		return err
	}

	return op.Run(stopC)
}

// getKubernetesClients returns all the required clients to communicate with
// kubernetes cluster: CRD type client, pod terminator types client, kubernetes core types client.
func (m *Main) getKubernetesClients() (podtermk8scli.Interface, crd.Interface, kubernetes.Interface, error) {
	var err error
	var cfg *rest.Config

	// If devel mode then use configuration flag path.
	if m.flags.Development {
		cfg, err = clientcmd.BuildConfigFromFlags("", m.flags.KubeConfig)
		if err != nil {
			return nil, nil, nil, fmt.Errorf("could not load configuration: %s", err)
		}
	} else {
		cfg, err = rest.InClusterConfig()
		if err != nil {
			return nil, nil, nil, fmt.Errorf("error loading kubernetes configuration inside cluster, check app is running outside kubernetes cluster or run in development mode: %s", err)
		}
	}

	// Create clients.
	k8sCli, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, nil, nil, err
	}

	// App CRD k8s types client.
	ptCli, err := podtermk8scli.NewForConfig(cfg)
	if err != nil {
		return nil, nil, nil, err
	}

	// CRD cli.
	aexCli, err := apiextensionscli.NewForConfig(cfg)
	if err != nil {
		return nil, nil, nil, err
	}
	crdCli := crd.NewClient(aexCli, m.logger)

	return ptCli, crdCli, k8sCli, nil
}

func main() {
	fmt.Println("----start main--------")


	logger := &applogger.Std{}

	stopC := make(chan struct{})
	finishC := make(chan error)
	signalC := make(chan os.Signal, 1)
	signal.Notify(signalC, syscall.SIGTERM, syscall.SIGINT)

	m := New(logger)

	// Run in background the operator.
	go func() {
		finishC <- m.Run(stopC)
	}()

	select {
	case err := <-finishC:
		if err != nil {
			fmt.Fprintf(os.Stderr, "error running operator: %s", err)
			os.Exit(1)
		}
	case <-signalC:
		logger.Infof("Signal captured, exiting...")
	}

}

// Flags are the controller flags.
type Flags struct {
	flagSet *flag.FlagSet

	ResyncSec   int
	KubeConfig  string
	Development bool
}

// OperatorConfig converts the command line flag arguments to operator configuration.
func (f *Flags) OperatorConfig() operator.Config {
	return operator.Config{
		ResyncPeriod: time.Duration(f.ResyncSec) * time.Second,
	}
}

// NewFlags returns a new Flags.
func NewFlags() *Flags {
	fmt.Println("--------init flag------")
	f := &Flags{
		flagSet: flag.NewFlagSet(os.Args[0], flag.ExitOnError),
	}
	// Get the user kubernetes configuration in it's home directory.
	kubehome := filepath.Join(homedir.HomeDir(), ".kube", "config")

	fmt.Println("--------end flag------")
	// Init flags.
	f.flagSet.IntVar(&f.ResyncSec, "resync-seconds", 30, "The number of seconds the controller will resync the resources")
	f.flagSet.StringVar(&f.KubeConfig, "kubeconfig", kubehome, "kubernetes configuration path, only used when development mode enabled")
	f.flagSet.BoolVar(&f.Development, "development", false, "development flag will allow to run the operator outside a kubernetes cluster")

	f.flagSet.Parse(os.Args[1:])

	return f
}